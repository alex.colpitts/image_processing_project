import cv2 as cv
import math
import matplotlib.pyplot as plt
import numpy as np

# custom imports
from pnseq import pn
from imagedisp import display_img_cv, display_img_py


class watermarking():
    def encode(img, data, seq):
        """
        Generate the watermark based on data, img, and seq
        """

        n_chips = len(seq)
        pixels = int(img.shape[0] * img.shape[1])
        n_data_bits = int(np.floor(pixels/n_chips))

        # generate random data
        if data == None:
            data = np.random.randint(0, 2, n_data_bits)

        data_chip = np.zeros([n_data_bits, n_chips], dtype=type(pn_seq))

        for i in range(data.size):
            if data[i] == 1:
                data_chip[i, :] = seq
            elif data[i] == 0:
                data_chip[i, :] = np.zeros(seq.shape, dtype=type(seq))

        # pad data to prevent partial sequences, reshape to image size
        data_1D = np.append(data_chip.flatten(),
                            np.zeros((np.mod(pixels, n_chips),)))
        watermark = np.reshape(data_1D, img.shape)

        return watermark, data

    def decode(img, seq):

        seq_len = len(seq)
        flat_img = img.flatten()

        n_symbols = int(np.floor(flat_img.size / seq_len))

        # reshape to be (n_symbols, seq_len), trim padded zeros
        seq_split = np.reshape(
            flat_img[0:int(n_symbols * seq_len)], (n_symbols, -1))

        data_est = np.zeros(n_symbols, dtype=np.float)

        max_corr = np.correlate(seq.astype(np.float), seq.astype(np.float))

        for i in range(n_symbols):
            data_est[i] = np.correlate(
                seq_split[i], seq.astype(np.float)) / max_corr

        return np.uint8(np.round(data_est))

    def lsb_zero(A):
        """
        Set all LSB to zero, assumes uint8 as element dtype
        """
        return np.bitwise_and(A.astype(np.uint8), np.uint8(np.ones(A.shape, dtype=np.uint8)*254))

    def msb_zero(A):
        return np.bitwise_and(A.astype(np.uint8), np.uint8(np.ones(A.shape, dtype=np.uint8)))


if __name__ == '__main__':

    np.random.seed(13)

    # load the image
    image_path = './images/bw_ex.jpg'
    img = cv.imread(image_path, cv.IMREAD_GRAYSCALE)
    img_no_lsb = watermarking.lsb_zero(img)

    # select sequence
    #pn_seq = pn.barker(13)
    pn_seq = np.array([1])

    # create the watermark
    watermark, data = watermarking.encode(img_no_lsb, data=None, seq=pn_seq)

    # threshold to prevent roll over in bytes
    img_watermarked = np.uint8(img_no_lsb + watermark)

    # retrieve the data from the watermarked image
    data_est = watermarking.decode(
        watermarking.msb_zero(img_watermarked), pn_seq)

    BER_MSE = (np.mean(data - data_est))**2

    if BER_MSE > 0:
        print(' '.join(['MSE', str(10*np.log10(BER_MSE)), 'dB']))

    print(np.mean(data), np.var(data))
    print(np.mean(watermarking.msb_zero(img)),
          np.var(watermarking.msb_zero(img)))

    # display_img_cv(img_watermarked)
    # display_img_py(img_watermarked)
