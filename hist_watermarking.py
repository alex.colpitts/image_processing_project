import cv2 as cv
import math
import matplotlib.pyplot as plt
import numpy as np

# custom imports
#from pnseq import pn
from imagedisp import display_img_cv, display_img_py

if __name__ == '__main__':

    # load the image
    #image_path = './images/bw_ex.jpg'
    #image_path = './images/mm.jpg'
    image_path = './images/banana.png'

    img = cv.imread(image_path, cv.IMREAD_GRAYSCALE)
    
    try:
        img_layers = img.shape[2]
    except IndexError:
        img_layers = 1

    print(img_layers)

    hist_list = np.zeros([img_layers, 256], dtype=np.int)

    for i in range(img_layers):
        hist_list[i, :] = np.squeeze(cv.calcHist([img],[i],None,[256],[0,256]))

    # unused histogram bins
    # CONSIDER ONLY ONE COLOR LAYER AT THIS POINT hist_list[0, :]
    empty_bins = []

    for hist_i in [0]:
        for i in range(0, 256):
            if hist_list[hist_i, i] == 0:
                empty_bins.append(i)

    unused_bins = len(empty_bins)
    used_bins = 256 - unused_bins

    if unused_bins > 0:
        C = np.int(np.log2(unused_bins))
        #print(U, C)

    sorted_histogram_index = np.argsort(hist_list[0, :])
    sorted_histogram = np.sort(hist_list[0, :])

    print(unused_bins, used_bins)




    #display_img_cv(img)