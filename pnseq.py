import math
from operator import inv
import numpy as np
import scipy.signal as sps


class pn():

    def heimiller(p):
        """
        p -> length parameter of Heimiller sequence
        """

        primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,
                  47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109]

        # if number is not prime, exit
        if not (p in primes):
            print('\np is not prime. Select another number and try again.')
            return None

        # initialize data structure
        chips = np.zeros(p**2, dtype=np.complex)

        # create chips
        i = 0
        for c in range(0, p):
            for r in range(0, p):
                chips[i] = (math.exp(1) ** (2 * math.pi * r * c / p * 1j))
                i += 1

        return chips

    def m_seq(p):
        pass

    def barker(p):

        # zero or -1 for barker
        mav = 1
        miv = -1

        valid_lengths = [2, 3, 4, 5, 7, 11, 13]

        if p not in valid_lengths:
            print(
                '\np is not a valid Barker Sequence length. Select another number and try again.')
            return None

        seq_dtype = np.float

        if p == 2:
            return np.array([mav, miv], dtype=seq_dtype)
        elif p == 3:
            return np.array([mav, mav, miv], dtype=seq_dtype)
        elif p == 4:
            return np.array([mav, mav, miv, mav], dtype=seq_dtype)
        elif p == 5:
            return np.array([mav, mav, mav, miv, mav], dtype=seq_dtype)
        elif p == 7:
            return np.array([mav, mav, mav, miv, miv, mav, miv], dtype=seq_dtype)
        elif p == 11:
            return np.array([mav, mav, mav, miv, miv, miv, mav, miv, miv, mav, miv], dtype=seq_dtype)
        elif p == 13:
            return np.array([mav, mav, mav, mav, mav, miv, miv, mav, mav, miv, mav, miv, mav], dtype=seq_dtype)

    def binary_seq_inv(seq):

        inv_seq = np.zeros(seq.shape, dtype=type(seq))

        for i in range(len(seq)):
            if seq[i] == 0:
                inv_seq[i] = 1
            elif seq[i] == 1:
                inv_seq[i] = 0
            else:
                print('Error: PN sequence was not binary.')
                exit()
             
        return inv_seq

    def gwn_seq(mu=0, var=1, p=22):
        return np.random.normal(mu, var, size=p), np.random.normal(mu, var, size=p)

    def br_seq(p=22):
        amp = 2
        return (np.random.randint(0, 2, size=p)*amp - 1), (np.random.randint(0, 2, size=p)*amp - 1)

    def m_seq(p=22):
        n_bits = np.ceil(np.log2(p + 1)).astype(np.int)
        seq_0 = sps.max_len_seq(n_bits, length=p, state=[1, 0, 1, 1, 1]) 
        seq_1 = sps.max_len_seq(n_bits, length=p, state=[0, 1, 0, 0, 1]) 

        return seq_0[0], seq_1[0]


if __name__ == '__main__':
    p1, p2 = pn.m_seq()