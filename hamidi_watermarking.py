import cv2 as cv
from numpy.core.fromnumeric import var
from numpy.lib.shape_base import tile
import scipy as sp
import scipy.signal as sps
import numpy as np
import matplotlib.pyplot as plt
from skimage.metrics import structural_similarity as ssim
import os

from pnseq import pn
from imagedisp import display_img_cv, display_img_py

class hamidi_watermarking:
    tw = 8

    def __init__(self):
        
        self.error = 0
        pass

    def pn_midband_masking(self, pn_seq, dct_loc=[4, 3]):
        
        # maximum number of diagonals is 15
        n_diag = dct_loc[0]
        diag_offset = dct_loc[1]
        
        if len(pn_seq) == 64:
            return np.reshape(pn_seq, (8, 8))

        else:
            pn_loc = []

            for i in range(self.tw):
                start = diag_offset-i
                stop = diag_offset-i+n_diag

                if start < 0:
                    start = 0

                if stop >= self.tw:
                    stop = self.tw 

                for j in range(start, stop):
                    pn_loc.append((i, j))
            
            mask = np.zeros((self.tw, self.tw), dtype=np.float)

            for i in range(len(pn_loc)):
                mask[pn_loc[i]] = pn_seq[i%len(pn_seq)]

            return mask

    def encoder(self, img_og, watermark, pn_0, pn_1, k=300, dct_mask=[4, 3]):

        img_og_size = np.array(img_og.shape)

        # prepare the watermark bit templates
        is_zero = self.pn_midband_masking(pn_0, dct_mask)
        is_one = self.pn_midband_masking(pn_1, dct_mask)
        data_mask = np.zeros(is_one.shape, dtype=type(is_one))

        # apply DFT, extract mag and phase data
        IMG_OG = sp.fft.fft2(img_og)
        IMG_OG_MAG = np.abs(IMG_OG)
        IMG_OG_ANG = np.angle(IMG_OG)
        
        # prepare to process DCT
        tiles = np.floor(img_og_size/self.tw).astype(np.int)
        IMG_MOD_MAG = np.zeros(IMG_OG_MAG.shape, dtype=type(IMG_OG_MAG))

        watermark = np.reshape(watermark, tiles)

        # split DFT into 8x8 blocks and iterate through them
        for i in range(tiles[0]):
            for j in range(tiles[1]):

                # identify which mask to add to the selected tile
                if watermark[i, j] == 0:
                    data_mask = is_zero
                elif watermark[i, j] == 1:
                    data_mask = is_one

                # find the DCT of the current tile
                tile_dct = sp.fft.dctn(IMG_OG_MAG[i*self.tw:i*self.tw + self.tw, j*self.tw:j*self.tw + self.tw])

                # add the mask to the DCT of the selected tile
                masked_tile = data_mask * k + tile_dct               
                
                # inverse DCT to obtain modified magnitude
                IMG_MOD_MAG[i*self.tw:i*self.tw + self.tw, j*self.tw:j*self.tw + self.tw] = \
                    sp.fft.idctn(masked_tile)

        # convert mag and phase back to complex
        IMG_MOD = IMG_MOD_MAG * np.exp(1j*IMG_OG_ANG)

        # ignore imaginary component since it is floating point error
        self.img_mod = np.uint8(sp.fft.ifft2(IMG_MOD.astype(np.complex)).real)

        return self.img_mod

    def decoder(self, img_mod, pn_0, pn_1, dct_mask=[4, 3]):

        self.error = 0

        img_mod_size = np.array(img_mod.shape)

        IMG_MOD = sp.fft.fft2(img_mod)
        IMG_MOD_MAG = np.abs(IMG_MOD)
        IMG_MOD_ANG = np.angle(IMG_MOD)
        
        # prepare the watermark bit templates
        pn_0_mask = self.pn_midband_masking(pn_0, dct_mask)
        pn_1_mask = self.pn_midband_masking(pn_1, dct_mask)
        pn_mask = self.pn_midband_masking(np.ones(pn_0.shape), dct_mask)

        # prepare to process DCT
        tiles = np.floor(img_mod_size/self.tw).astype(np.int)

        water_est = np.array([])

        # split DFT into 8x8 blocks and iterate through them
        for i in range(tiles[0]):
            for j in range(tiles[1]):

                # find DCT of the magnitude tile
                tile_dct = sp.fft.dctn(IMG_MOD_MAG[i*self.tw:i*self.tw + self.tw, j*self.tw:j*self.tw + self.tw])

                # correlate against both masks
                mask_corr_0 = sps.correlate2d(tile_dct, pn_0_mask, 'valid')
                mask_corr_1 = sps.correlate2d(tile_dct, pn_1_mask, 'valid')

                if mask_corr_0 > mask_corr_1:
                    bit = 0
                elif mask_corr_1 > mask_corr_0:
                    bit = 1
                else:
                    bit = np.NaN
                    
                # append the latest bit
                water_est = np.append(water_est, bit)

                if bit != self.og_bits[i, j]:
                    #print((i, j))
                    self.error += 1
                    pass

        return water_est


def seq_selection(seq_type, seq_inv):
    # pn_seq setup
    pn_seq_0 = 0
    pn_seq_1 = 0

    if seq_type == 'Gaussian':
        pn_seq_0, pn_seq_1 = pn.gwn_seq(mu=0, var=1, p=n_chips)

    elif seq_type == 'Binary':
        pn_seq_0, pn_seq_1 = pn.br_seq(n_chips)
        pass

    elif seq_type == 'Barker':
        pn_seq_0 = pn.barker(13)
        pn_seq_1 = -pn_seq_0

    if seq_inv is True:
        pn_seq_1 = -pn_seq_0
    
    return pn_seq_0, pn_seq_1

if __name__ == '__main__':

    np.random.seed(13)

    plot_on = False

    # setup logging
    log_file = './results.txt'
    log_writer = open(log_file, 'w')

    # load in images
    image_path = './images/cameraman.tif'
    img = cv.imread(image_path, cv.IMREAD_GRAYSCALE)

    # random bitstream generation
    watermark_bits = np.int(np.floor(img.shape[0]/hamidi_watermarking.tw) * np.floor(img.shape[1]/hamidi_watermarking.tw))
    test_water = np.random.randint(0, 2, watermark_bits)

    #seq_type = ['Barker', 'Binary', 'Gaussian']
    #seq_inv = [True, False]
    seq_type = ['Gaussian']
    seq_inv = [True]
    diag_offsets = 15
    diag_count = 15

    n_chips = 100

    # create watermarking object instance
    ham = hamidi_watermarking()
    print(ham.pn_midband_masking(np.ones(n_chips)))
    #exit()
    for d_offset in range(diag_offsets):
        for d_count in range(diag_count-d_offset):
            for selected_seq in seq_type:
                for seq_bool in seq_inv:

                    # select sequence based on iteration
                    pn_seq_0, pn_seq_1 = seq_selection(selected_seq, seq_inv=seq_bool)    

                    # add original bits to the instance
                    ham.og_bits = np.reshape(test_water, (np.floor(img.shape[0]/8).astype(np.int), np.floor(img.shape[1]/8).astype(np.int)))

                    iters = 100
                    k_max = 100000
                    MSE = np.zeros(iters)
                    BER = np.zeros(iters)
                    SSIM = np.zeros(iters)
                    pn_gain = np.linspace(1, k_max, iters)

                    for i in range(iters):
                        
                        # encode and decode watermarks
                        img_mod = ham.encoder(img, test_water, pn_seq_0, pn_seq_1, k=pn_gain[i], dct_mask=[d_count, d_offset]) 
                        recovered_watermark = ham.decoder(img_mod, pn_seq_0, pn_seq_1, dct_mask=[d_count, d_offset]).astype(np.int)

                        MSE[i] = 10*np.log10(np.mean((img - img_mod)**2, axis=None))
                        BER[i] = 10*np.log10(ham.error/watermark_bits)
                        SSIM[i] = ssim(img, img_mod, data_range=img_mod.max() - img_mod.min())

                        #display_img_py(img_mod)

                    # optionally plot and save figures                    
                    if plot_on == True:
                        plt.figure()
                        plt.plot(pn_gain, BER, pn_gain, MSE, pn_gain, (SSIM*100-100))
                        plt.legend(['BER (dB)', 'MSE (dB)', 'SSIM'])
                        plt.xlabel('k')
                        plt.savefig(os.path.join(os.getcwd(), 'figures', selected_seq + str(seq_bool) + '.png'))

                    log_writer.write(' '.join([selected_seq + str(seq_bool), str((d_offset, d_count)), str(np.min(BER)), '\n']))
                    print(d_offset, d_count)
    
        #plt.show()

    log_writer.close()