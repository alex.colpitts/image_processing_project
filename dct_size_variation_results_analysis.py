import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    results_file = 'dct_size_variation_results.txt'

    df = pd.DataFrame(columns=['seq_type', 'diag_count', 'diag_offset', 'ber_db'])

    dummy_dict = {}

    with open(results_file, 'r+') as f:
        line = True
        while line:
            line = f.readline()
            cleaned_line = line.replace(', ', ',').strip('\n').split(' ')
            

            if len(cleaned_line) == 4:
                temp = cleaned_line[1].strip('()').split(',')

                #print(cleaned_line[0:3])
                dummy_dict['seq_type'] = cleaned_line[0]
                dummy_dict['diag_count'] = np.int(temp[0])
                dummy_dict['diag_offset'] = np.int(temp[1])
                dummy_dict['ber_db'] = np.float(cleaned_line[2])
                 
                df = df.append(dummy_dict, ignore_index=True)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_trisurf(df['diag_count'], df['diag_offset'], df['ber_db'])
    ax.view_init(elev=19, azim=34)
    ax.set_xlabel('Number of Diagonals')
    ax.set_ylabel('Diagonal Offset')
    ax.set_zlabel('BER (dB)')
    plt.savefig('./figures/mask_size_variation.png')