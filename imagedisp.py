import cv2 as cv
import matplotlib.pyplot as plt


def display_img_cv(img):
    cv.namedWindow('image', cv.WINDOW_AUTOSIZE)
    cv.imshow('image', img)
    cv.waitKey()


def display_img_py(img):
    plt.figure()
    plt.imshow(img, cmap='gray', vmin=0, vmax=255)
    plt.show()
